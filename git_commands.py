print("Criando um arquivo novo")
print("Gitlab é melhor que Github")

print("Bitbucket é melhor que Gitlab! Porém, Github é melhor que Bitbucket!")

# Branch - Uma ramificação da versão principal do código (main/master)
# Commit - Um registro de uma versão do código, contendo que alterações foram realizadas (adição, alteração ou deleção), em que data, e por quem

# git clone <link para o repositório> -> Criar uma cópia local do repositório
# git pull -> Traz alterações feitas por outros colaboradores no repositório
# git add . -> Adiciona todas as alterações feitas para o novo commit
# git commit -m "mensagem" -> Registra o commit
# git config --global credential.github.com.useHttpPath true -> Garante que as credencias para o gitlab vão ser requisitadas na hora do push
# git push -> Envia o comit para o repositório online